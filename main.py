from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram import ChatAction, KeyboardButton, ReplyKeyboardMarkup, ReplyKeyboardRemove
import sqlite3
import json
import random

sql_file = '/home/ubuntu/telegram-bot/db.sqlite'

admins = ('igorsob', 'vyacheslavsaloid_personal')

updater = Updater('774398348:AAExN0qGfVpPa4f94wXHTwfXBULSDs5mHck')
# updater = Updater('734089062:AAEFXD7Y11YZAKCbP83k1GtwDT7E1S7fSUg')
dispatcher = updater.dispatcher

keyboard = [['Музыка'], ['Спорт'], ['Путешествия'], ['Фильмы'], ['Книги'], ['Игры']]
interests_in_keyboard = ['Музыка', 'Спорт', 'Путешествия', 'Фильмы', 'Книги', 'Игры']

speakers = [
    "Татьяна Бутенко - Muses Code JS or How Communities Change People",
    "Борис Могила - Parallel calls in node.js",
    "Asim Hussain - The Future of Machine Learning & JavaScript",
    "Андрей Михайлов - Deploying enterprise HyperLedger block-chains",
    "Liran Tal - Black Clouds and Silver Linings in Node.js Security",
    "Николай Лотоцкий - Deploying Node.js applications to AWS",
    "Luciano Mammino - It’s about time to embrace Streams",
    "Александра Калинина - You don't need Lodash, do you?",
    "Виктор Турский - The working architecture of NodeJs applications",
    "Maksym Demydenko - Using Blockchain in Node.js project: JavaScript Ninja’s experience",
    "Вячеслав Паневский - Node.js and Serverless",
    "Александр Хохлов - ClojureScript journey: From little script, to CLI program, to AWS Lambda function",
    "Jamie Maria Schouren - The search for App-iness : Progressive Web Apps",
    "Виталий Гурин - Пікові навантаження в чат-бот проектах. Історія боротьби",
    "Виталий Ратушный - Definition of Done: Deadline driven development",
]


stickers = {'hello': 'CAADAgADFwADaBHODTOl30LAUu-iAg',
            'bye': 'CAADAgADGAADaBHODcNK9WgZp0KdAg',
            'question': 'CAADAgADGwADaBHODczy_MawXCpQAg',
            'thank': 'CAADAgADGgADaBHODXi-OsAQSFSFAg',
            'love': 'CAADAgADGQADaBHODej6z46pOa_jAg'}


class User:

    def __init__(self, chat_id, username=None, first_name=None, last_name=None, new=False):
        self.conn = sqlite3.connect(sql_file)
        self.cursor = self.conn.cursor()
        if new:
            self.chat_id = chat_id
            self.answers = '{"1":false,"2":false,"3":false}'
            self.interests = '[]'
            self.stage = 0
            self.team = 0
            sql = '''insert into user (chat_id, username, first_name, last_name, answers, stage, interests, team) 
            values (?, ?, ?, ?, ?, ?, ?, ?);'''
            with self.conn:
                try:
                    self.cursor.execute(sql, (chat_id, username, first_name, last_name,
                                              self.answers, self.stage, self.interests, self.team))
                except Exception as e:
                    print(e)
            self.id = self.cursor.lastrowid
            self.username = username
            self.first_name = first_name
            self.last_name = last_name
        else:
            self.chat_id = chat_id
            sql = '''select id, username, first_name, last_name, answers, interests, stage, team 
            from user where chat_id=?;'''
            self.cursor.execute(sql, (self.chat_id, ))
            row = self.cursor.fetchall()[0]

            (self.id, self.username, self.first_name, self.last_name,
             self.answers, self.interests, self.stage, self.team) = row
            self.answers = json.loads(self.answers)
            self.interests = json.loads(self.interests)

    def update_state(self, state):
        sql = 'update user set stage=? where chat_id=?;'
        with self.conn:
            self.cursor.execute(sql, (state, self.chat_id))

    def add_interest(self, interest):
        sql = 'update user set interests=? where chat_id=?;'
        self.interests.append(interest)
        interests = json.dumps(self.interests)
        with self.conn:
            self.cursor.execute(sql, (interests, self.chat_id))


def send_networking_command(bot, update):
    if update.message.from_user.username not in admins:
        return
    conn = sqlite3.connect(sql_file)
    cursor = conn.cursor()
    sql = 'select id, chat_id, username, first_name, last_name, answers, interests, stage from user;'
    cursor.execute(sql)
    users = [user for user in cursor.fetchall() if len(json.loads(user[6])) != 0 and user[2] is not None]
    interests = {interest: [] for interest in interests_in_keyboard}
    for user in users:
        try:
            interests[json.loads(user[6])[0]].append({"chat_id": user[1], "username": user[2]})
        except:
            pass
    for interest, user_list in interests.items():
        for user in user_list:
            if len(user_list) == 1:
                while True:
                    interests_str = interests_in_keyboard.copy()
                    interests_str.remove(interest)
                    new_interest = interests_str[random.randint(0, len(interests_str)-1)]
                    if len(interests[new_interest]) >= len(user_list):
                        to = random.randint(0, len(interests[new_interest]) - 1)
                        try:
                            bot.send_message(chat_id=user['chat_id'],
                                             text=f'Оказывается у меня слабоваты знания в теме "{interest}". '
                                             f'Но я познакомился с очень классным человеком, который увлекается темой "{new_interest}" '
                                             f'Кидаю тебе его ник: @{interests[new_interest][to]["username"]} '
                                             f'Напиши ему "Привет, я от UniQа... давай встретимся и пообщаемся" '
                                             f'Он поймет)')
                        except:
                            pass
                        break
                continue
            u = user_list[:]
            u.remove(user)
            to = random.randint(0, len(u)-1)
            try:
                bot.send_message(chat_id=user['chat_id'],
                                 text=f'Я помню, ты указывал, что тебе интересно поговорить на тему "{interest}". \n'
                                 f'Я тут познакомился с очень классным человеком, который тоже этим увлекается. \n'
                                 f'Кидаю тебе его ник: @{u[to]["username"]} напиши ему, что ты от меня: ')
                bot.send_message(chat_id=user['chat_id'], text=f'Привет, я от UniQа... Давай встретимся и пообщаемся)')
                bot.send_message(chat_id=user['chat_id'], text=f'Он поймет')
            except:
                pass


send_networking_command_handler = CommandHandler('send_networking', send_networking_command)
dispatcher.add_handler(send_networking_command_handler)


def send_all_command(bot, update):
    if update.message.from_user.username not in admins:
        return
    update.message.text.replace(' ', '')
    conn = sqlite3.connect(sql_file)
    cursor = conn.cursor()
    sql = 'select chat_id from user;'
    cursor.execute(sql)
    chat_ids = [user[0] for user in cursor.fetchall()]
    for chat_id in chat_ids:
        try:
            bot.send_message(chat_id=chat_id, text=update.message.text.replace('/send_all ', ''))
        except:
            pass


send_all_command_handler = CommandHandler('send_all', send_all_command)
dispatcher.add_handler(send_all_command_handler)


def send_start_speech_command(bot, update):
    if update.message.from_user.username not in admins:
        return
    conn = sqlite3.connect(sql_file)
    cursor = conn.cursor()
    sql = 'select chat_id from user;'
    cursor.execute(sql)
    chat_ids = [user[0] for user in cursor.fetchall()]
    for chat_id in chat_ids:
        try:
            bot.send_message(chat_id=chat_id, text='Start speech')
        except:
            pass


send_start_speech_command_handler = CommandHandler('send_start_speech', send_start_speech_command)
dispatcher.add_handler(send_start_speech_command_handler)


def start_task_1_command(bot, update):
    if update.message.from_user.username not in admins:
        return
    conn = sqlite3.connect(sql_file)
    cursor = conn.cursor()
    sql = 'select chat_id from user;'
    cursor.execute(sql)
    chat_ids = [user[0] for user in cursor.fetchall()]
    texts = ['Ты свободен? Я очень запутался. Пришел на обед, заказал себе Цезарь. '
             'А мне принесли какой-то листок с непонятной надписью '
             '"Ънлчмцз чыфсацдт бицъ цитыс нмсцчхдбфнццсучк, цн ьшьъыс нлч, кнме чйвнцсн - уфжа у ьъшнюь!", '
             'и на ухо официант сказал цифру. \n'
             'Цифру я уже не помню, но, к счастью, я назвал ее моему соседу \n'
             'Помоги понять 😓',
             'Ты свободен? Я очень запутался. Пришел на обед, заказал себе Цезарь. '
             'А мне принесли какой-то листок с непонятной надписью, и на ухо официант сказал цифру "24". \n'
             'Текст я уже не помню, но, к счастью, листок я отдал моему соседу. \n'
             'Помоги понять 😓']
    for chat_id in chat_ids:
        team = random.randint(0, 1)
        text = texts[team]
        sql = 'update user set team=?, stage=1 where chat_id=?;'
        with conn:
            cursor.execute(sql, (team, chat_id))
        try:
            bot.send_sticker(chat_id, stickers['question'])
            bot.send_message(chat_id=chat_id, text=text)
        except:
            pass


start_task_1_command_handler = CommandHandler('start_task_1', start_task_1_command)
dispatcher.add_handler(start_task_1_command_handler)


def start_task_2_command(bot, update):
    if update.message.from_user.username not in admins:
        return
    conn = sqlite3.connect(sql_file)
    cursor = conn.cursor()
    sql = 'select chat_id from user;'
    cursor.execute(sql)
    chat_ids = [user[0] for user in cursor.fetchall()]
    texts = ['Ты тут? Мне срочно нужна твоя помощь. Команда A', 'Ты тут? Мне срочно нужна твоя помощь. Команда Б']
    for chat_id in chat_ids:
        team = random.randint(0, 1)
        text = texts[team]
        sql = 'update user set team=?, stage=2 where chat_id=?;'
        with conn:
            cursor.execute(sql, (team, chat_id))
        bot.send_sticker(chat_id, stickers['question'])
        bot.send_message(chat_id=chat_id, text=text)


start_task_2_command_handler = CommandHandler('start_task_2', start_task_2_command)
dispatcher.add_handler(start_task_2_command_handler)


def start_task_3_command(bot, update):
    if update.message.from_user.username not in admins:
        return
    conn = sqlite3.connect(sql_file)
    cursor = conn.cursor()
    sql = 'select chat_id from user;'
    cursor.execute(sql)
    chat_ids = [user[0] for user in cursor.fetchall()]
    texts = ['Ты тут? Мне срочно нужна твоя помощь. Команда A', 'Ты тут? Мне срочно нужна твоя помощь. Команда Б']
    for chat_id in chat_ids:
        team = random.randint(0, 1)
        text = texts[team]
        sql = 'update user set team=?, stage=3 where chat_id=?;'
        with conn:
            cursor.execute(sql, (team, chat_id))
        bot.send_sticker(chat_id, stickers['question'])
        bot.send_message(chat_id=chat_id, text=text)


start_task_3_command_handler = CommandHandler('start_task_3', start_task_3_command)
dispatcher.add_handler(start_task_3_command_handler)


def start_command(bot, update):
    if update.message.from_user.username in admins:
        bot.send_message(chat_id=update.message.chat_id,
                         text='️❕❗⛔ WARNING ⛔❗❕\n\n'
                              'Обязательно к прочтению!'
                              'Команды сразу отправляют сообщения всем пользователям. \n'
                              'Команда "/ send_all" только на крайний случай. (Без пробела)\n'
                              'Синтаксис: \n'
                              '/ send_all Текст который придет пользователям',
                         reply_markup=ReplyKeyboardMarkup([['/send_networking', '/send_start_speech'],
                                                           ['/start_task_1', '/start_feedback', '/start_bot_feedback'],
                                                           ['/finish']]))
        return

    bot.send_sticker(update.message.chat_id, stickers['hello'])

    user = User(update.message.chat_id,
                update.message.from_user.username,
                update.message.from_user.first_name,
                update.message.from_user.last_name,
                new=True)

    bot.send_message(chat_id=update.message.chat_id,
                     text='Привет, меня зовут UniQ и я буду твоим ассисентом на конференции NodeUkraine 2019.')
    bot.send_message(chat_id=update.message.chat_id,
                     text='Очень хочу узнать тебя получше... '
                          'Мог бы ты выбрать тему, на которую с тобой можно поговорить?',
                     reply_markup=ReplyKeyboardMarkup(keyboard, one_time_keyboard=True))


start_command_handler = CommandHandler('start', start_command)
dispatcher.add_handler(start_command_handler)


def text_message(bot, update):
    user = User(update.message.chat_id)

    if user.stage == 0:  # Заполнение интересов
        if update.message.text in interests_in_keyboard:
            user.update_state(5)
            user.add_interest(update.message.text)
            bot.send_message(chat_id=update.message.chat_id,
                             text='Воу... '
                                  'Это очень круто... '
                                  'Мы с тобой еще обязательно пообщаемся, а сейчас я убегаю на первый доклад.',
                             reply_markup=ReplyKeyboardRemove())
        else:
            bot.send_message(chat_id=update.message.chat_id, text='К сожалению я знаю только темы что на кнопках...(',
                             reply_markup=ReplyKeyboardMarkup(keyboard, one_time_keyboard=True))

    elif user.stage == 1:  # Задание 1
        text = 'Сегодня отличный шанс найти единомышленников, не упусти его, ведь общение - ключ к успеху!'
        if update.message.text == text:
            user.answers["1"] = True
            sql = 'update user set answers=?, stage=5 where chat_id=?;'
            with user.conn:
                user.cursor.execute(sql, (json.dumps(user.answers), user.chat_id))
            bot.send_message(chat_id=update.message.chat_id, text='Спасибо тебе огрмоное, до связи)')
            bot.send_sticker(update.message.chat_id, stickers['thank'])
        else:
            bot.send_message(chat_id=update.message.chat_id, text='Все еще не понимаю(')

    elif user.stage == 2:  # Задание 2
        if (update.message.text == 'test' and user.team == 0) or (update.message.text == 'test' and user.team == 1):
            user.answers["2"] = True
            sql = 'update user set answers=?, stage=5 where chat_id=?;'
            with user.conn:
                user.cursor.execute(sql, (json.dumps(user.answers), user.chat_id))
            bot.send_message(chat_id=update.message.chat_id, text='Спасибо тебе огрмоное, до связи)')
            bot.send_sticker(update.message.chat_id, stickers['thank'])
        else:
            bot.send_message(chat_id=update.message.chat_id, text='Пробовал подставить значения - выдает ошибку. '
                                                                  'Что-то пошло не так?')

    elif user.stage == 3:  # Задание 3
        if (update.message.text == 'test' and user.team == 0) or (update.message.text == 'test' and user.team == 1):
            user.answers["3"] = True
            sql = 'update user set answers=?, stage=5 where chat_id=?;'
            with user.conn:
                user.cursor.execute(sql, (json.dumps(user.answers), user.chat_id))
            bot.send_message(chat_id=update.message.chat_id, text='Спасибо тебе огрмоное, до связи)')
            bot.send_sticker(update.message.chat_id, stickers['thank'])
        else:
            bot.send_message(chat_id=update.message.chat_id, text='Пробовал подставить значения - выдает ошибку. '
                                                                  'Что-то пошло не так?')

    elif user.stage == 6:  # Фидбек
        user.answers["6"] = update.message.text
        sql = 'update user set answers=?, stage=7 where chat_id=?;'
        with user.conn:
            user.cursor.execute(sql, (json.dumps(user.answers), user.chat_id))
        bot.send_message(chat_id=update.message.chat_id, text='Топ 3 спикеров, которых ты можешь выделить')

    elif user.stage == 7:  # Фидбек
        user.answers["7"] = update.message.text
        sql = 'update user set answers=?, stage=8 where chat_id=?;'
        with user.conn:
            user.cursor.execute(sql, (json.dumps(user.answers), user.chat_id))
        bot.send_message(chat_id=update.message.chat_id, text='Ты бы хотел посетить NodeUkraine в следующем году?')

    elif user.stage == 8:  # Фидбек
        user.answers["8"] = update.message.text
        sql = 'update user set answers=?, stage=9 where chat_id=?;'
        with user.conn:
            user.cursor.execute(sql, (json.dumps(user.answers), user.chat_id))
        bot.send_message(chat_id=update.message.chat_id, text='Возможно у тебя есть '
                                                              'предложения/комментарии/рекомендации по конференции')

    elif user.stage == 9:  # Фидбек
        user.answers["9"] = update.message.text
        sql = 'update user set answers=?, stage=5 where chat_id=?;'
        with user.conn:
            user.cursor.execute(sql, (json.dumps(user.answers), user.chat_id))
        bot.send_sticker(update.message.chat_id, stickers['love'])
        bot.send_message(chat_id=update.message.chat_id, text='Спасибо тебе огрмоное)')

    elif user.stage == 10:  # Фидбек бот
        user.answers["10"] = update.message.text
        sql = 'update user set answers=?, stage=11 where chat_id=?;'
        with user.conn:
            user.cursor.execute(sql, (json.dumps(user.answers), user.chat_id))
        bot.send_message(chat_id=update.message.chat_id, text='Чему мне следует научиться?')

    elif user.stage == 11:  # Фидбек бот
        user.answers["11"] = update.message.text
        sql = 'update user set answers=?, stage=5 where chat_id=?;'
        with user.conn:
            user.cursor.execute(sql, (json.dumps(user.answers), user.chat_id))
        bot.send_sticker(update.message.chat_id, stickers['love'])
        bot.send_message(chat_id=update.message.chat_id, text='Спасибо тебе огрмоное)')

    elif user.stage == 5:  # Простой
        bot.send_message(chat_id=update.message.chat_id, text='Я сейчас очень занят, напишу тебе чуть позже)')


text_message_handler = MessageHandler(Filters.text, text_message)
dispatcher.add_handler(text_message_handler)


def start_bot_feedback_command(bot, update):
    if update.message.from_user.username not in admins:
        return
    conn = sqlite3.connect(sql_file)
    cursor = conn.cursor()
    sql = 'select chat_id from user;'
    cursor.execute(sql)
    chat_ids = [user[0] for user in cursor.fetchall()]
    for chat_id in chat_ids:
        sql = 'update user set stage=10 where chat_id=?;'
        with conn:
            cursor.execute(sql, (chat_id, ))
        try:
            bot.send_message(chat_id=chat_id, text='Йоу, очень хотел бы узнать твоё мнение о моей работе сегодня...\n'
                                                   'Напиши, пожалуйста\n'
                                                   'Чем я был тебе полезен?')
        except:
            pass


start_bot_feedback_command_handler = CommandHandler('start_bot_feedback', start_bot_feedback_command)
dispatcher.add_handler(start_bot_feedback_command_handler)


def start_feedback_command(bot, update):
    if update.message.from_user.username not in admins:
        return
    conn = sqlite3.connect(sql_file)
    cursor = conn.cursor()
    sql = 'select chat_id from user;'
    cursor.execute(sql)
    chat_ids = [user[0] for user in cursor.fetchall()]
    for chat_id in chat_ids:
        sql = 'update user set stage=6 where chat_id=?;'
        with conn:
            cursor.execute(sql, (chat_id, ))
        try:
            bot.send_message(chat_id=chat_id, text='Тадам✨ \n'
                                                   'Поздравляю тебя - ты стал участником второй '
                                                   'конференции NodeUkraine. ' 
                                                   'Мне очень приятно, что ты присоединился, буду рад новым '
                                                   'встречам на Nodeukraine и OdessaJS. \n\n\n'
                                                   'Ответь, пожалуйста, на нексколько вопросов: \n'
                                                   'Что тебе понравилось больше всего на конференции NodeUkraine?')
        except:
            pass


start_feedback_command_handler = CommandHandler('start_feedback', start_feedback_command)
dispatcher.add_handler(start_feedback_command_handler)


def finish_command(bot, update):
    if update.message.from_user.username not in admins:
        return
    conn = sqlite3.connect(sql_file)
    cursor = conn.cursor()
    sql = 'select chat_id from user;'
    cursor.execute(sql)
    chat_ids = [user[0] for user in cursor.fetchall()]
    for chat_id in chat_ids:
        sql = 'update user set stage=5 where chat_id=?;'
        with conn:
            cursor.execute(sql, (chat_id, ))
        try:
            bot.send_sticker(chat_id, stickers['bye'])
            bot.send_message(chat_id=chat_id,
                             text='Спасибо тебе огромное за помощь. Был рад с тобой познакомиться. \n'
                                  'На выходе ты сможешь найди мой стенд и получить от меня небольшой презент. \n'
                                  'До встречи на следующих конференциях) ')
        except:
            pass


finish_command_handler = CommandHandler('finish', finish_command)
dispatcher.add_handler(finish_command_handler)


updater.start_polling(clean=True)
updater.idle()
